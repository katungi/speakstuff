import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

class RedPage extends StatefulWidget {
  const RedPage({Key key}) : super(key: key);

  @override
  _RedPageState createState() => _RedPageState();
}

enum TtsState { playing, stopped, paused, continued }

class _RedPageState extends State<RedPage> {
  FlutterTts flutterTts = FlutterTts();
  TtsState ttsState = TtsState.stopped;

  @override
  void initState() {
    _speak();
    super.initState();
  }

  @override
  void dispose() {
    _stop();
    super.dispose();
  }

  Future _speak() async {
    var result = await flutterTts.speak('Here is the red screen');
    if (result == 1)
      setState(() {
        ttsState = TtsState.playing;
      });
    await flutterTts.setVoice({"name": "Karen", "locale": "en-US"});
    await flutterTts.setSilence(2);
    await flutterTts.setSpeechRate(1.0);
    await flutterTts.setVolume(1.0);
    await flutterTts.setPitch(1.0);
  }

  Future _stop() async {
    var result = await flutterTts.stop();
    if (result == 1) setState(() => ttsState = TtsState.stopped);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Red Page'),
        backgroundColor: Colors.red,
      ),
      body: Container(
        color: Colors.red,
      ),
    );
    ;
  }
}
