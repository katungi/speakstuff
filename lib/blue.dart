import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

class BluePage extends StatefulWidget {
  const BluePage({Key key}) : super(key: key);

  @override
  _BluePageState createState() => _BluePageState();
}

enum TtsState { playing, stopped, paused, continued }

class _BluePageState extends State<BluePage> {
  FlutterTts flutterTts = FlutterTts();
  TtsState ttsState = TtsState.stopped;
  @override
  void initState() {
    _speak();
    super.initState();
  }

  @override
  void dispose() {
    _stop();
    super.dispose();
  }

  Future _speak() async {
    var result = await flutterTts.speak('Here is the blue screen');
    if (result == 1)
      setState(() {
        ttsState = TtsState.playing;
      });
    await flutterTts.setVoice({"name": "Karen", "locale": "en-US"});
    await flutterTts.setSilence(2);
    await flutterTts.setSpeechRate(1.0);
    await flutterTts.setVolume(1.0);
    await flutterTts.setPitch(1.0);
  }

  Future _stop() async {
    var result = await flutterTts.stop();
    if (result == 1) setState(() => ttsState = TtsState.stopped);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Blue Page'),
        backgroundColor: Colors.blue,
      ),
      body: Container(
        color: Colors.blue,
      ),
    );
  }
}
